from django import forms
from .models import Schedule

class NewAppointment(forms.Form):
    appointment = forms.CharField(label="Appointment", max_length=50)
    date = forms.DateField()
    start_time = forms.TimeField()
    end_time = forms.TimeField()