# Generated by Django 2.2.6 on 2019-10-06 07:51

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20191006_1442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 10, 6, 14, 51, 38, 439477)),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 10, 6, 14, 51, 38, 439477)),
        ),
    ]
