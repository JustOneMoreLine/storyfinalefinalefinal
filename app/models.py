from django.db import models
import datetime

# Create your models here.
class Schedule(models.Model):
    appointment = models.CharField(max_length=50)
    date = models.DateField(default=datetime.date.today)
    start_time = models.TimeField()
    end_time = models.TimeField()

    def getAll():
        allObjects = Schedule._meta.model.objects.all()
        result = [i for i in allObjects]
        return result

    def getDate(self):
         return str(self.date)
    
    def getStartTime(self):
        return self.start_time

    def getEndTime(self):
        return self.end_time
    
    def getAppointmentName(self):
        return str(self.appointment)
    
    def getTime(self):
        return str(self.start_time) + " - " + str(self.end_time)

        
